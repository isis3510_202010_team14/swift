//
//  InterfaceController.swift
//  eTriageWatch Extension
//
//  Created by Nicolás Moreno on 1/04/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit
import WatchConnectivity

class InterfaceController: WKInterfaceController {
    
    var healthStore: HKHealthStore?
    var lastHeartRate = 0.0
    let session = WCSession.default
    @IBOutlet weak var bpmLabel: WKInterfaceLabel!
    @IBOutlet weak var heartRateSpeedLabel: WKInterfaceLabel!
    let beatCountPerMinute = HKUnit(from: "count/min")
    
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        let sampleType: Set<HKSampleType> = [HKSampleType.quantityType(forIdentifier: .heartRate)!]
            
        healthStore = HKHealthStore()
            
        healthStore?.requestAuthorization(toShare: sampleType, read: sampleType, completion: { (success, error) in
            if success {
                self.startHeartRateQuery(quantityTypeIdentifier: .heartRate)
            }
        })
        
        session.delegate = self
        session.activate()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func startHeartRateQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier) {
        // 1
        let devicePredicate = HKQuery.predicateForObjects(from: [HKDevice.local()])
        
        // 2
        let updateHandler: (HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, Error?) -> Void = {
            query, samples, deletedObjects, queryAnchor, error in
            
        // 3
            guard let samples = samples as? [HKQuantitySample] else { return }
            
            self.process(samples, type: quantityTypeIdentifier)
        }
        
        // 4
        let query = HKAnchoredObjectQuery(type: HKObjectType.quantityType(forIdentifier: quantityTypeIdentifier)!, predicate: devicePredicate, anchor: nil, limit: HKObjectQueryNoLimit, resultsHandler: updateHandler)
        
        query.updateHandler = updateHandler
        
        // 5
        healthStore?.execute(query)
    }
       
       private func process(_ samples: [HKQuantitySample], type: HKQuantityTypeIdentifier) {
            for sample in samples {
                if type == .heartRate {
                    lastHeartRate = sample.quantity.doubleValue(for: beatCountPerMinute)
                    print("❤ Last heart rate was: \(lastHeartRate)")
                }
                
                updateHeartRateLabel()
                updateHeartRateSpeedLabel()
            }
       }
       
       private func updateHeartRateLabel() {
            let heartRate = String(Int(lastHeartRate))
            bpmLabel.setText(heartRate)
       }
       
       private func updateHeartRateSpeedLabel() {
           switch lastHeartRate {
           case _ where lastHeartRate > 130:
               heartRateSpeedLabel.setText("High")
               heartRateSpeedLabel.setTextColor(.red)
           case _ where lastHeartRate > 100:
               heartRateSpeedLabel.setText("Moderate")
               heartRateSpeedLabel.setTextColor(.yellow)
           default:
               heartRateSpeedLabel.setText("Low")
               heartRateSpeedLabel.setTextColor(.blue)
           }
       }
    
    
    @IBAction func tapSendToiPhone() {
        let heartRate = String(Int(lastHeartRate))
        let data: [String: Any] = ["watch": heartRate as Any] //Create your dictionary as per uses
        //let data: [String: Any] = ["watch": "75"]
        session.sendMessage(data, replyHandler: nil, errorHandler: nil)
    }
    
}

extension InterfaceController: WCSessionDelegate {
  
  func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
  }
  
  func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
    
    print("received data: \(message)")
    if let value = message["iPhone"] as? String {
        self.bpmLabel.setText(value)
    }
  }
}
