//
//  HistorialTableViewCell.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 26/04/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit

class HistorialTableViewCell: UITableViewCell {

    @IBOutlet weak var clCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension HistorialTableViewCell{
    func setColletionViewDataSourceDelegate
        <D: UICollectionViewDelegate & UICollectionViewDataSource>
        (_ dataSourceDelegate: D, forRow row: Int)
            {
                clCollectionView.delegate = dataSourceDelegate
                clCollectionView.dataSource = dataSourceDelegate
                
                clCollectionView.reloadData()
            }
}

