//
//  ResultadoViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 29/02/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Lottie

class ResultadoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate {

    @IBOutlet weak var sintomas: UITableView!
    var sintomasA: [String] = []
    
    var cedula: String = ""
    
    let animationView = AnimationView()
    
    @IBOutlet weak var prioridadLabel: UILabel!
    @IBOutlet weak var tiempoAd: UILabel!
    @IBOutlet weak var animation: UIView!
    @IBOutlet weak var textResultado: UILabel!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        setupAnimation()
        self.textResultado.text = "Agrega sintomas y recalcula el triage del paciente, de acuerdo a los signos vitales la prioridad es baja"
    }
    
    func setupAnimation(){
        animationView.animation = Animation.named("wait")
        animationView.frame = CGRect(x: 0, y: 0, width: 180, height: 170)
        //animationView.center = view.center
        //animationView.backgroundColor = .
        animationView.contentMode = .scaleToFill
        animationView.loopMode = .loop
        animationView.play()
        self.animation.addSubview(animationView)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sintomasA.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel!.text = sintomasA[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
        print("Deleted")

        self.sintomasA.remove(at: indexPath.row)
        self.sintomas.deleteRows(at: [indexPath], with: .automatic)
        
        if sintomasA.count == 0 {
            animation.isHidden = false
        }
      }
    }

    
    @IBAction func addSintoma(_ sender: Any) {

    }
    
    private func addNewToDoItem(title: String)
    {

        let newIndex = sintomasA.count


       sintomasA.append(title)


        sintomas.insertRows(at: [IndexPath(row: newIndex, section: 0)], with: .top)
    }
    
    
    @IBAction func triageCal(_ sender: Any) {
        
        var triage = 0.0
        
        for sintoma in sintomasA {
            triage += triageWeigth(sintoma: sintoma)
        }
        
        if triage < 0.3 {
            prioridadLabel.text = "Baja"
            tiempoAd.text = "40 minutos"
            self.textResultado.text = "El paciente presenta sintomas leves, no necesita atención urgente"
            
        } else if triage > 0.3 && triage < 0.7{
            prioridadLabel.text = "Media"
            tiempoAd.text = "25 minutos"
            self.textResultado.text = "El paciente presenta algunos sintomas de alerta, se recomienda seguimiento del paciente"
        }
        else if triage > 0.7 {
            prioridadLabel.text = "Alta"
            tiempoAd.text = "10 minutos"
            self.textResultado.text = "El paciente presenta sintomas graves, relacionados a gripes fuertes, Coronavirus o Influenza"
        }
        
        let docref = db.collection("Paciente").document(cedula)
        
        if sintomasA.count == 0{
            docref.updateData(["Prioridad":prioridadLabel.text!]) { (err) in
                print("Error al update")
            }
        } else{
            docref.updateData(["Sintomas":sintomasA, "Prioridad":prioridadLabel.text!]) { (err) in
                print("Error al update")
            }
        }
        
        
        
    }
    
    func triageWeigth(sintoma: String) -> Double {
        
        var weigth = 0.0
        switch sintoma {
        case "Fiebre":
            weigth = weigth + 0.2
        case "Tos":
            weigth = weigth + 0.4
        case "Dolor de Cabeza":
            weigth = weigth + 0.3
        case "Dolor de Garganta":
            weigth = weigth + 0.2
        case "Cansancio/Fatiga":
            weigth = weigth + 0.3
        case "Dolor Muscular":
            weigth = weigth + 0.1
        case "Congestión Nasal":
            weigth = weigth + 0.2
        case "Diarrea":
            weigth = weigth + 0.2
        default:
            weigth = weigth + 0.0
        }
        
        return weigth
    }
}
