//
//  CargandoViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 28/02/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import Lottie
import WatchConnectivity
import SVProgressHUD

class CargandoViewController: UIViewController, WCSessionDelegate {
    
    let animationView = AnimationView()
    
    var cedula:String = ""
    
    @IBOutlet weak var animacion: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAnimation()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Cancelar(_ sender: Any) {
        
        let alert = UIAlertController(title: "Cancelar Lectura", message: "Esta seguro que desea salir?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Continuar", style: UIAlertAction.Style.default, handler: nil))
       
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupAnimation(){
        animationView.animation = Animation.named("19392-card-loading")
        
        let screenSize: CGRect = UIScreen.main.bounds
        print(screenSize.height)
        print(screenSize.width)
        if screenSize.height < 700.0 {
            animationView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        }else{
            animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        }
        animationView.contentMode = .scaleToFill
        animationView.loopMode = .loop
        animationView.play()
        self.animacion.addSubview(animationView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! SignosVitalesViewController
        
        vc.cedula = self.cedula
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if WCSession.isSupported() { // check if the device support to handle an Apple Watch
            let session = WCSession.default
            session.delegate = self
            session.activate() // activate the session

            if session.isPaired { // Check if the iPhone is paired with the Apple Watch
                    // Do stuff
                SVProgressHUD.showSuccess(withStatus: "Conexión al Apple Watch")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self.performSegue(withIdentifier: "toSign", sender: nil)
                }
                
            }else{
                SVProgressHUD.show(withStatus: "Inicia eTriageWatch en el Apple Watch.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                    self.performSegue(withIdentifier: "toSign", sender: nil)
                }
            }
        }
    }

}
