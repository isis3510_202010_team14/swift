//
//  SintomasViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 24/04/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import Lottie

class SintomasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    
    let sintomas = ["Fiebre", "Tos", "Dolor de Cabeza", "Dolor de Garganta", "Cansancio/Fatiga", "Dolor Muscular", "Congestión Nasal", "Diarrea"]
    var searchdata: [String]! = []
    
    var selectedSintomas: [String]! = []
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchbar: UISearchBar!
    
    let animationView = AnimationView()
    
    @IBOutlet weak var animacion: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.searchdata = sintomas

        self.tableview.allowsMultipleSelectionDuringEditing = true
        self.tableview.setEditing(true, animated: false)
        
        self.searchbar.delegate = self

        setupAnimation()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSintoma", for: indexPath) as! SintomaTableViewCell
        
        cell.sintoma.text = searchdata[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! SintomaTableViewCell

        selectedSintomas.append(currentCell.sintoma!.text!)
        
        print(currentCell.sintoma!.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.text = ""
        searchdata = sintomas
        searchBar.endEditing(true)
        tableview.reloadData()
    }

    

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchdata = searchText.isEmpty ? sintomas : sintomas.filter {
                    (item: String) -> Bool in
                    return item.range(of: searchText, options: .caseInsensitive, range: nil,locale: nil) != nil
                }
        tableview.reloadData()
    }
    
    func setupAnimation(){
        animationView.animation = Animation.named("arrow")
        animationView.frame = CGRect(x: 0, y: 0, width: 69, height: 69)
        animationView.contentMode = .scaleToFill
        animationView.loopMode = .loop
        animationView.play()
        self.animacion.addSubview(animationView)
    }
    
    @IBAction func agregar(_ sender: Any) {
        print(selectedSintomas!)

        if let presenter = presentingViewController as? ResultadoViewController {
            for i in selectedSintomas {
                presenter.sintomasA.append(i)
            }
            presenter.sintomas.reloadData()
            if selectedSintomas.count > 0{
                presenter.animation.isHidden = true
            }
            
        }
        if let presenter = presentingViewController as? HistorialViewController {
            for i in selectedSintomas {
                presenter.sintomasData.append(i)
            }
            presenter.tableView.reloadData()
            
        }
        dismiss(animated: true)
    }
    
}
