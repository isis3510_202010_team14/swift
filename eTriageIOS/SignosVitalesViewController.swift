//
//  SignosVitalesViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 5/04/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import WatchConnectivity
import FirebaseFirestore
import SVProgressHUD

class SignosVitalesViewController: UIViewController, UITextFieldDelegate {
    
    //Conectar label ritmo cardiaco
    let db = Firestore.firestore()
    
    @IBOutlet weak var FrecuenciaCardiaca: UILabel!
    @IBOutlet weak var alturafield: UITextField!
    var session: WCSession?
    
    var cedula:String = ""
    
    @IBOutlet weak var FC: UILabel!
    @IBOutlet weak var SO: UITextField!
    @IBOutlet weak var PA: UITextField!
    @IBOutlet weak var PC: UITextField!
    @IBOutlet weak var AL: UITextField!
    @IBOutlet weak var TC: UITextField!
    
    override func viewDidLoad() {
        
        SVProgressHUD.show(withStatus: "Cargando Datos del Paciente")
        
        
        SO.delegate = self
        PA.delegate = self
        PC.delegate = self
        AL.delegate = self
        TC.delegate = self
        
        super.viewDidLoad()
        self.configureWatchKitSession()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        let docRef = db.collection("Paciente").document(cedula)
        
        
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                
                let nombre = document.get("Altura") as! String
                self.alturafield.text = String(nombre)
                print("Document data: \(dataDescription)")
                SVProgressHUD.showSuccess(withStatus: "Listo")
            } else {
                print("Document does not exist")
                SVProgressHUD.showError(withStatus: "Error al cargar los datos")
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                           replacementString string: String) -> Bool
    {
        var maxLength = 0
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == SO{maxLength = 2}
        else if textField == PA{maxLength = 3}
        else if textField == PC{maxLength = 2}
        else if textField == AL{maxLength = 3}
        else if textField == TC{maxLength = 4}
        return newString.length <= maxLength
    }
    
    
    func configureWatchKitSession() {
      
      if WCSession.isSupported() {
        session = WCSession.default
        session?.delegate = self
        session?.activate()
      }
    }

    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ResultadoViewController
        
        vc.cedula = self.cedula
    }

}

extension SignosVitalesViewController: WCSessionDelegate {
  
  func sessionDidBecomeInactive(_ session: WCSession) {
  }
  
  func sessionDidDeactivate(_ session: WCSession) {
  }
  
  func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
  }
  
  func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
    print("received message: \(message)")
    DispatchQueue.main.async {
      if let value = message["watch"] as? String {
        self.FrecuenciaCardiaca.text = value
      }
    }
  }
}
