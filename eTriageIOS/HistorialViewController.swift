//
//  HistorialViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 22/04/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import Lottie
import SVProgressHUD
import FirebaseFirestore

class HistorialViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let animationView = AnimationView()

    let db = Firestore.firestore()
    
    @IBOutlet weak var animation: UIView!
    
    @IBOutlet weak var fechahora: UILabel!
    @IBOutlet weak var DocumentoN: UILabel!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var rh: UILabel!
    @IBOutlet weak var icono: UIImageView!
    @IBOutlet weak var textPrioridad: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewE: UITableView!
    
    var nombreN: String?
    var documentoN: String?
    var codeN: String?
    var fechahoraN: String?
    var rhN: String?
    var prioridad: String?
    
    var row: Int = 0
    
    var sintomas: [String?] = []
    var sintomasData: [String?] = []
    var actividades: [String?] = []
    var actividadesData: [String?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show(withStatus: "Cargando Datos del Paciente")
        
        nombre.text = nombreN
        DocumentoN.text = documentoN
        code.text = codeN
        fechahora.text = fechahoraN
        rh.text = rhN
        
        
        if prioridad == "Baja" {
            textPrioridad.text = "El paciente presenta un estado de prioridad baja"
            icono.tintColor = UIColor(displayP3Red: 26, green: 11, blue: 220, alpha: 1)
        } else if prioridad == "Media"{
            textPrioridad.text = "El paciente presenta un estado de prioridad media"
            icono.tintColor = UIColor(displayP3Red: 255, green: 204, blue: 0, alpha: 1)
        } else if prioridad == "Alta"{
            textPrioridad.text = "El paciente presenta un estado de prioridad alta"
            icono.tintColor = UIColor(displayP3Red: 253, green: 59, blue: 48, alpha: 1)
        }
        
        setupAnimation()
        
        
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()
        
        
        
    }
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
            let alert = UIAlertController(title: "No hay Conexión a Internet", message: "No es posible traer los datos del paciente", preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true)
            SVProgressHUD.showError(withStatus: "No hay conexión a internet no fue posible traer los datos del paciente")
        case .wifi:
            print("wifi")
            datosPaciente()
        case .wwan:
            print("wan")
           datosPaciente()
            
        }
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    func datosPaciente() {
        
        SVProgressHUD.show(withStatus: "Cargando información del Paciente")
        
        let docRef = db.collection("Paciente").document(DocumentoN.text!)
            
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                
                let nombre = document.get("Nombre") as! String
                let doc = document.get("Documento") as! String
                let fecha = document.get("Fecha") as! String
                let code = document.get("Code") as! String
                let rh = document.get("RH") as! String
                let prioridad = document.get("Prioridad") as! String
                self.nombre.text = String(nombre)
                self.DocumentoN.text = String(doc)
                self.fechahora.text = String(fecha)
                self.code.text = String(code)
                self.rh.text = String(rh)
                self.prioridad = String(prioridad)
                let sintomas = document.get("Sintomas") as! [String]
                self.sintomas = sintomas
                let actividades = document.get("Actividades") as! [String]
                self.actividades = actividades
                
                if prioridad == "Baja" {
                    self.textPrioridad.text = "El paciente presenta un estado de prioridad baja"
                    //detailVC.icono.tintColor = UIColor(displayP3Red: 26, green: 11, blue: 220, alpha: 1)
                    self.icono.tintColor = UIColor.blue
                } else if prioridad == "Media"{
                    self.textPrioridad.text = "El paciente presenta un estado de prioridad media"
                    self.icono.tintColor = UIColor(displayP3Red: 255, green: 204, blue: 0, alpha: 1)
                } else if prioridad == "Alta"{
                    self.textPrioridad.text = "El paciente presenta un estado de prioridad alta"
                    self.icono.tintColor = UIColor.red
                }

                SVProgressHUD.dismiss()
                self.sintomasData = self.sintomas
                self.actividadesData = self.actividades
                
                self.tableView.reloadData()
                
                SVProgressHUD.showSuccess(withStatus: "listo")
                print("Document data: \(dataDescription)")
            } else {
                print("Document does not exist")
            }
        }
    }
    
    func setupAnimation(){
        animationView.animation = Animation.named("cross")
        animationView.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        animationView.contentMode = .scaleToFill
        animationView.loopMode = .loop
        animationView.play()
        self.animation.addSubview(animationView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHistorial") as! HistorialTableViewCell
            cell.clCollectionView.reloadData()
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0;//Choose your custom row height
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sintomasData.count
  
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellInsideHistorial", for: indexPath) as! HistorialCollectionViewCell
                       cell.sintoma.text = sintomasData[indexPath.row]
                   return cell
        
       
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
  
            self.sintomasData.remove(at: indexPath.row)
            collectionView.deleteItems(at: [indexPath])

        
    }
    
    @IBAction func addEx(_ sender: Any) {
        switch Network.reachability.status {
        case .unreachable:
            let alert = UIAlertController(title: "No hay Conexión a Internet", message: "No es posible traer los datos del paciente", preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true)
            SVProgressHUD.showError(withStatus: "No hay conexión a internet no fue posible traer los datos del paciente")
            
            let userDefaults = UserDefaults.standard
            let sintomas = sintomasData
            userDefaults.set(sintomas, forKey: "Sintomas")
            print(sintomas)
            
        case .wifi:
            print("wifi")
            datosPaciente()
        case .wwan:
            print("wan")
           datosPaciente()
            
        }
    }
    
    
    @IBAction func addSin(_ sender: Any) {
        switch Network.reachability.status {
        case .unreachable:
            let alert = UIAlertController(title: "No hay Conexión a Internet", message: "No es posible traer los datos del paciente", preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true)
            SVProgressHUD.showError(withStatus: "No hay conexión a internet no fue posible traer los datos del paciente")
        case .wifi:
            print("wifi")
            datosPaciente()
        case .wwan:
            print("wan")
           datosPaciente()
            
        }
    }
    
}

extension HistorialViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: 134, height: 93)
        return size

    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }

    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    
    
}
