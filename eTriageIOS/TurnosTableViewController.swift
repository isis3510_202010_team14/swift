//
//  TurnosTableViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 29/02/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import FirebaseFirestore
import SVProgressHUD

struct Paciente {
    var sexo: String
    var documento: String
    var edad: String
    var nombre: String
    var ciudad: String
    var altura: String
    var rh: String
    var code: String
    var prioridad: String
    var sintomas: [String]
    var fecha: Date
    var actividades: [String]
}

class TurnosTableViewController: UITableViewController {
    
    let Historial = ["Pacientes Graves", "Pacientes estables"]
    let imagenes = ["azul.png","gota.png"]
    
    let db = Firestore.firestore()
    
    var contador = 0
    
    var pacientes = [Paciente]()

    override func viewDidLoad() {
        super.viewDidLoad()
         self.tableView.estimatedRowHeight = 115
        contador = 0;
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()
    }
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
            let alert = UIAlertController(title: "No hay Conexión a Internet", message: "No es posible traer la lista de Turnos del Triage", preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
                //self.continuar.isEnabled = false
            }))
            self.present(alert, animated: true)
        case .wifi:
            print("wifi")
            
        case .wwan:
            print("wan")
           
            
        }
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if contador < 1{
            SVProgressHUD.show(withStatus: "Cargando Pacientes")
            Firestore.firestore().collection("Paciente").getDocuments{ (snapshot, error) in
                if let err = error{
                    print("Error al traer los datos: \(err)")
                    SVProgressHUD.showError(withStatus: "No se pudo traer los datos")
                } else {
                    for document in (snapshot?.documents)! {
                        let data = document.data()
                        let sexo = data["Sexo"] as? String
                        let documento = data["Documento"] as? String
                        let edad = data["Edad"] as? String
                        let nombre = data["Nombre"] as? String
                        let ciudad = data["Ciudad"] as? String
                        let altura = data["Altura"] as? String
                        let rh = data["RH"] as? String
                        let code = data["Code"] as? String
                        let prioridad = data["Prioridad"] as? String
                        let sintomas = data["Sintomas"] as? [String]
                        let dia = data["Fecha"] as! String
                        let dateFormatterGet = DateFormatter()
                        dateFormatterGet.dateFormat = "dd/MM/yyyy HH:mm:ss"
                        let fecha = dateFormatterGet.date(from: dia)
                        let actividades = data["Actividades"] as? [String]
                        let pacienteNew = Paciente(sexo: sexo!, documento: documento!, edad: edad!, nombre: nombre!, ciudad: ciudad!, altura: altura!, rh: rh!, code: code!, prioridad: prioridad!, sintomas: sintomas!, fecha: fecha!, actividades: actividades! )
                        self.pacientes.append(pacienteNew)
                    }
                    
                    self.tableView.reloadData()
                    self.contador = self.contador + 1
                    SVProgressHUD.dismiss()
                }
            }
        }
        if contador == 0{
            SVProgressHUD.showError(withStatus: "No hay pacientes registrados")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toHistorial"{
            let detailVC: HistorialViewController = segue.destination as! HistorialViewController
            let cell: PacienteTableViewCell = sender as! PacienteTableViewCell
            
            let id: String? = cell.cedulaPaciente.text
            
            detailVC.documentoN = id
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pacientes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTurno", for: indexPath) as! PacienteTableViewCell
       
        cell.configureCell(paciente: pacientes[indexPath.row])
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0;//Choose your custom row height
    }


}


