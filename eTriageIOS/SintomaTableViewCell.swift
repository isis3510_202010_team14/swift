//
//  SintomaTableViewCell.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 24/04/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit

class SintomaTableViewCell: UITableViewCell {

    @IBOutlet weak var sintoma: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }

}
