//
//  ExamenTableViewCell.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 26/04/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit

class ExamenTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var clColletionView2: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ExamenTableViewCell{
    func setColletionViewDataSourceDelegate
        <D: UICollectionViewDelegate & UICollectionViewDataSource>
        (_ dataSourceDelegate: D, forRow row: Int)
            {
                clColletionView2.delegate = dataSourceDelegate
                clColletionView2.dataSource = dataSourceDelegate
                
                clColletionView2.reloadData()
            }
}
