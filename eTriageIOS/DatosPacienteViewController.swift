//
//  DatosPacienteViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 28/02/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import Lottie
import FirebaseFirestore
import Firebase

class DatosPacienteViewController: UIViewController, UITextFieldDelegate {
    
    let animationView = AnimationView()
    
    let db = Firestore.firestore()
    
    @IBOutlet weak var pacienteAnimation: UIView!
    @IBOutlet weak var continuar: UIButton!
    
    @IBOutlet weak var RH: UITextField!
    @IBOutlet weak var NOMBRE: UITextField!
    @IBOutlet weak var CEDULA: UITextField!
    @IBOutlet weak var SEXO: UITextField!
    @IBOutlet weak var EDAD: UITextField!
    @IBOutlet weak var ALTURA: UITextField!
    @IBOutlet weak var QRCODE: UILabel!
    @IBOutlet weak var CIUDAD: UITextField!
    
    var nombre = "Nombre"
    var rh = "O+"
    var cedula = "1022434816"
    var sexo = "M"
    var edad = "21"
    var altura = "178"
    var qrcode = "21321321312312"
    var ciudad = "Bogota DC"
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Comfortaa", size: 16)!]

        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        
        
        setupAnimation()

        RH.text = rh
        RH.delegate = self
        NOMBRE.text = nombre
        NOMBRE.delegate = self
        CEDULA.text = cedula
        CEDULA.delegate = self
        SEXO.text = sexo
        SEXO.delegate = self
        EDAD.text = edad
        EDAD.delegate = self
        ALTURA.text = altura
        ALTURA.delegate = self
        QRCODE.text = qrcode
        
        CIUDAD.text = ciudad
        CIUDAD.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()
        
        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                           replacementString string: String) -> Bool
    {
        var maxLength = 0
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == RH{maxLength = 3}
        else if textField == NOMBRE{maxLength = 100}
        else if textField == CEDULA{maxLength = 11}
        else if textField == SEXO{maxLength = 1}
        else if textField == EDAD{maxLength = 3}
        else if textField == ALTURA{maxLength = 3}
        else if textField == CIUDAD{maxLength = 40}
        return newString.length <= maxLength
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
            let alert = UIAlertController(title: "No hay Conexión a Internet", message: "Todos los datos del paciente se guardarán de forma local en el dispositivo hasta que haya conexión a Internet.", preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
                //self.continuar.isEnabled = false
            }))
            self.present(alert, animated: true)
        case .wifi:
            print("wifi")
            self.continuar.isEnabled = true
        case .wwan:
            print("wan")
            self.continuar.isEnabled = true
            
        }
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    func setupAnimation(){
        let screenSize: CGRect = UIScreen.main.bounds
        print(screenSize.height)
        print(screenSize.width)
        animationView.animation = Animation.named("paciente")
        if screenSize.height < 700.0 {
            animationView.frame = CGRect(x: 0, y: 0, width: 120, height: 110)
        }else{
            animationView.frame = CGRect(x: 0, y: 0, width: 190, height: 180)
        }
        animationView.contentMode = .scaleToFill
        animationView.loopMode = .loop
        animationView.play()
        self.pacienteAnimation.addSubview(animationView)
    }
    
    @IBAction func Seguir(_ sender: Any) {
        
        switch Network.reachability.status {
        case .unreachable:
            let userDefaults = UserDefaults.standard
            let paciente = [
                "Nombre": nombre,
                "RH": rh,
                "Cedula": cedula,
                "Sexo": sexo,
                "Edad": edad,
                "Altura": altura,
                "QRCode": qrcode,
                "Ciudad": ciudad
            ]
            userDefaults.set(paciente, forKey: "Paciente")
            print(paciente)
        case .wifi:
            print("wifi")
            self.continuar.isEnabled = true
            
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            
            
            db.collection("Paciente").document("\(CEDULA.text!)").setData(["Nombre":NOMBRE.text!, "Documento":CEDULA.text!, "RH": RH.text!, "Sexo": SEXO.text!, "Edad": EDAD.text!, "Altura": ALTURA.text!, "Code": QRCODE.text!, "Ciudad": CIUDAD.text!, "Sintomas":[], "Prioridad":"Sin asignar", "Fecha":dateFormatter.string(from: date), "Actividades":[]])
            
            Analytics.logEvent("Patient_Admitted", parameters: [
            "document": cedula,
            "name": nombre
            ])
            
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: "id-Patient",
            AnalyticsParameterItemName: "Patient_Admitted",
            AnalyticsParameterContentType: "cont"
            ])
            
        case .wwan:
            print("wan")
            self.continuar.isEnabled = true
            
        }
        
    }
    
    @IBAction func pacienteAgregado(_ sender: UIButton)
    {
        Analytics.logEvent("paciente_agregado", parameters: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! CargandoViewController
        
        vc.cedula = CEDULA.text!
    }

}
