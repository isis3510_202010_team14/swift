//
//  ViewController.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 27/02/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import Lottie
import SVProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet weak var animationViewContainer: UIView!
    @IBOutlet weak var admisionBtn: UIButton!
    @IBOutlet weak var turnoBtn: UIButton!
    let animationView = AnimationView()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()
        setupAnimation()
    }
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
            let alert = UIAlertController(title: "No hay Conexión a Internet", message: "No hay conexión, todos los datos serán guardados en el celular, no podras usar la lectura de documentos", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: nil))

            self.present(alert, animated: true)
        case .wwan:
            SVProgressHUD.showSuccess(withStatus: "Conectado a Plan de Datos")
        case .wifi:
            //SVProgressHUD.showSuccess(withStatus: "Conectado a Internet")
            print("wifi")
        }
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }

    func setupAnimation(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        print(screenSize.height)
        print(screenSize.width)
        
        animationView.animation = Animation.named("doctor")
        if screenSize.height < 700.0 {
            animationView.frame = CGRect(x: 0, y: 0, width: 350, height: 330)
        }else{
            animationView.frame = CGRect(x: 0, y: 0, width: 410, height: 410)
        }
        animationView.contentMode = .scaleToFill
        animationView.loopMode = .loop
        animationView.play()
        self.animationViewContainer.addSubview(animationView)
    }

}

