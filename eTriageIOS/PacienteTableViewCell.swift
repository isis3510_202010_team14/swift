//
//  PacienteTableViewCell.swift
//  eTriageIOS
//
//  Created by Nicolás Moreno on 29/02/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit

class PacienteTableViewCell: UITableViewCell {

    @IBOutlet weak var nombrePaciente: UILabel!
    @IBOutlet weak var edadPrioridadPaciente: UILabel!
    @IBOutlet weak var cedulaPaciente: UILabel!
    @IBOutlet weak var Prioridad: UILabel!
    @IBOutlet weak var fecha: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(paciente: Paciente) {
        nombrePaciente.text = paciente.nombre
        edadPrioridadPaciente.text = paciente.edad
        cedulaPaciente.text = paciente.documento
        Prioridad.text = paciente.prioridad
        
        let date = paciente.fecha
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        fecha.text = dateFormatter.string(from: date)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
