//
//  DocumentoViewController.swift
//  eTriageIOS
//0000
//  Created by Nicolás Moreno on 28/02/20.
//  Copyright © 2020 Moviles. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseMLVision
import SVProgressHUD
import FirebaseFirestore
import HealthKit
import Lottie

class DocumentoViewController: UIViewController, AVCapturePhotoCaptureDelegate {
    
    //:MARK
    var textRecognizer: VisionTextRecognizer!
    var datosPacienteTraseros: [String] = []
    var datosPacienteFrontales: [String] = []
    var numeroCaptura = 0
    
    let db = Firestore.firestore()
    
    let animationView = AnimationView()
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var TakePhotoBtn: UIButton!
    @IBOutlet weak var ejemplocedula: UIImageView!
    @IBOutlet weak var continuar: UIButton!
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    var navigationBarAppearace = UINavigationBar.appearance()
    var color = UIColor(red: 245, green: 245, blue: 245, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Comfortaa", size: 16)!]
        
        //Inicializar OCR
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()

        let vision = Vision.vision()
        textRecognizer = vision.onDeviceTextRecognizer()
        // Do any additional setup after loading the view.
        // guardar localmente
        //let defaults = UserDefaults.standard
        //defaults.set("hola", forKey: "Palabra")
        //let string = defaults.string(forKey: "Palabra")
        continuar.isEnabled = false
    }
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
            let alert = UIAlertController(title: "No hay Conexión a Internet", message: "No es posible hacer la lectura del documento, si desea continuar ingrese los datos del paciente de forma manual.", preferredStyle: .alert)

            //animationView.animation = Animation.named("rex")
            //animationView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            //--animationView.center = view.center
            //--animationView.backgroundColor = .
            //animationView.contentMode = .scaleToFill
            //animationView.loopMode = .loop
            //animationView.play()
            //self.pacienteAnimation.addSubview(animationView)
            
            //alert.view.addSubview(animationView)
            
            alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
                self.performSegue(withIdentifier: "ToDatosPaciente", sender: nil)
            }))
            self.present(alert, animated: true)
        case .wifi:
            print("wifi")
        case .wwan:
            print("wan")
            
        }
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    
    func runTextRecognition(with image: UIImage){
        let visionImage = VisionImage(image: image)
        textRecognizer.process(visionImage) { (features, error) in
            self.processResult(from: features, error: error)
            //SVProgressHUD.showSuccess(withStatus: "Done")
        }
    }
    
    func processResult (from text: VisionText? ,error: Error?){
        guard let features = text else {
            
            SVProgressHUD.show(withStatus: "Leyendo Documento...")
            
            if text == nil{
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Intenta de nuevo")
                numeroCaptura = numeroCaptura - 1
            }
        return}
 
        if numeroCaptura == 1{
            for block in features.blocks{
                for line in block.lines{
                    for element in line.elements{
                        print("texto: \(element.text)")
                        datosPacienteFrontales.append(element.text)
                    }
                }
            }
            if datosPacienteFrontales.count < 15{
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "No se detecto el documento, intente de nuevo")
                numeroCaptura = numeroCaptura - 1
            }
            else{
                print(datosPacienteFrontales)
                SVProgressHUD.showSuccess(withStatus: "Gira tu documento")
                self.ejemplocedula.image = UIImage(named: "cedulaTrasera.png")
            }
        }
        if numeroCaptura == 2{
            for block in features.blocks{
                for line in block.lines{
                    for element in line.elements{
                        print("texto: \(element.text)")
                        datosPacienteTraseros.append(element.text)
                    }
                }
            }
            if datosPacienteTraseros.count < 12{
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "No se detecto el documento, intente de nuevo")
                numeroCaptura = numeroCaptura - 1
            }
            else{
                print(datosPacienteTraseros)
                TakePhotoBtn.isEnabled = false
                continuar.isEnabled = true
                SVProgressHUD.showSuccess(withStatus: "Todo listo!")
            }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Volver"
        backItem.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont(name: "Comfortaa", size: 16)!],
        for: .normal)
        navigationItem.backBarButtonItem = backItem
        
        let vc = segue.destination as! DatosPacienteViewController
        
        if datosPacienteFrontales.count > 0{
            print("segue: \(datosPacienteFrontales[9])")
            let cedulaPre = datosPacienteFrontales[9]
            let newString = cedulaPre.replacingOccurrences(of: ",", with: ".", options: .literal, range: nil)
            vc.cedula = newString
            let apellidos = datosPacienteFrontales[10] + " " + datosPacienteFrontales[11]
            let nombres = datosPacienteFrontales[13] + " " + datosPacienteFrontales[14]
            vc.nombre = nombres + " " + apellidos
        }
    
        if datosPacienteTraseros.count > 0{
            let fechaNacimiento = datosPacienteTraseros[0]
            let año1a = fechaNacimiento.components(separatedBy: "-")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            if año1a.count > 2{
                let date = dateFormatter.date(from:año1a[2])!
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year, .month, .day, .hour], from: date)
                let date2 = Date()
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy"
                let hoy = calendar.dateComponents([.year, .month, .day, .hour], from: date2)
                let ano1 = components.year
                let ano2 = hoy.year
                vc.edad = String(ano2!-ano1!) + " Años"
            }
            else{
                vc.edad = "Edad"
            }
            
            vc.ciudad = datosPacienteTraseros[4]
            let sexo = datosPacienteTraseros[13]
            if sexo == "M"{
                vc.sexo = "Masculino"
            }
            if sexo == "F"{
                vc.sexo = "Femenino"
            }
            vc.altura = datosPacienteTraseros[10]
            vc.rh = datosPacienteTraseros[12]
            vc.qrcode = datosPacienteTraseros[datosPacienteTraseros.count-1]
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }

        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    
    }
    
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
    }
    
    @IBAction func didTakePhoto(_ sender: Any) {
        
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        settings.flashMode = .on
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        let image = UIImage(data: imageData)!
        //captureImageView.image = image
        numeroCaptura = numeroCaptura + 1
        runTextRecognition(with: image)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }

}

